#include <cstdint>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

using Source = std::string;
using Int = signed long long;
using byte = uint8_t;

byte& access(Int pc, std::vector<byte>& posTape, std::vector<byte>& negTape)
{
    if (pc >= 0)
    {
        size_t p = pc;
        while (p >= posTape.size()) posTape.push_back(0);
        return posTape[p];
    }
    size_t p = -pc;
    while (p >= negTape.size()) negTape.push_back(0);
    return negTape[p];
}

void moveForward(size_t& ip, const Source& source)
{
    size_t match = 0;
    while (ip+1 < source.size())
    {
        ++ip;
        if (source[ip] == '[') ++match;
        if (source[ip] == ']')
        {
            if (!match) return;
            --match;
        }
    }
}

void moveBackward(size_t& ip, const Source& source)
{
    size_t match = 0;
    while (ip > 1)
    {
        --ip;
        if (source[ip] == ']') ++match;
        if (source[ip] == '[')
        {
            if (!match) return;
            --match;
        }
    }
}

void interpret(const Source& source, std::istream& in, std::ostream& out)
{
    std::vector<byte> posTape;
    std::vector<byte> negTape;
    Int pc = 0;
    size_t ip = 0;
    while (ip < source.size())
    {
        switch (source[ip])
        {
        case '<': --pc; break;
        case '>': ++pc; break;
        case '+': ++access(pc, posTape, negTape); break;
        case '-': --access(pc, posTape, negTape); break;
        case '.': out << (char)access(pc, posTape, negTape); break;
        case ',':
            if (in.good())
            {
                in >> access(pc, posTape, negTape);
            }
            else
            {
                access(pc, posTape, negTape) = 0;
            }
            break;
        case '[':
            if (!access(pc, posTape, negTape))
            {
                moveForward(ip, source);
            }
            break;
        case ']':
            if (access(pc, posTape, negTape))
            {
                moveBackward(ip, source);
            }
            break;
        }
        ++ip;
    }
}

Source extract(std::istream& in)
{
    std::string reader;
    Source src;
    while (std::getline(in, reader))
    {
        for (auto c : reader)
        {
            switch (c)
            {
            case '<': case '>': case '+': case '-':
            case '.': case ',': case '[': case ']':
                src.push_back(c);
            default:
                ;
            }
        }
    }
    return src;
}

int main(int argc, char* argv[])
{
    for (int i = 1; i < argc; ++i)
    {
        std::ifstream file(argv[i]);
        if (!file.is_open())
        {
            std::cerr << "Could not open \"" << argv[i] << "\" for reading.\n";
            return 2;
        }
        auto source = extract(file);
        interpret(source, std::cin, std::cout);
    }
    if (argc <= 1)
    {
        auto source = extract(std::cin);
        interpret(source, std::cin, std::cout);
    }
}

