#include <iostream>
#include <fstream>
#include <random>
#include <stack>
#include <string>
#include <vector>

using Source = std::vector<std::string>;
using Int = signed long int;

enum Direction
{
    Left = 0,
    Right = 1,
    Up = 2,
    Down = 3
};

char get(const Source& source, size_t x, size_t y)
{
    if (y >= source.size()) return '\0';
    if (x >= source[y].size()) return '\0';
    return source[y][x];
}

void set(Source& source, size_t x, size_t y, char c)
{
    while (source.size() <= y) source.push_back("");
    while (source[y].size() <= x) source[y].push_back(' ');
    source[y][x] = c;
}

bool exists(const Source& source, size_t x, size_t y)
{
    return get(source, x, y) != '\0';
}

void move(const Source& source, size_t& x, size_t& y, Direction dir)
{
    switch (dir)
    {
    case Left: x = (x + source[y].size() - 1) % source[y].size(); break;
    case Right: x = (x + 1) % source[y].size(); break;
    case Up:
        do
        {
            y = (source.size() + y - 1) % source.size();
        } while (!exists(source, x, y));
        break;
    case Down:
        do
        {
            y = (y + 1) % source.size();
        } while (!exists(source, x, y));
        break;
    }
}

Int pop(std::stack<Int>& stack)
{
    if (stack.empty()) return 0;
    Int v = stack.top();
    stack.pop();
    return v;
}

void duplicate(std::stack<Int>& stack)
{
    if (stack.empty()) stack.push(0);
    else stack.push(stack.top());
}

// Behaviour for empty or one-element stacks not defined.
void swap(std::stack<Int>& stack)
{
    Int a = pop(stack);
    Int b = pop(stack);
    stack.push(a);
    stack.push(b);
}

void interpret(Source& source, std::istream& in, std::ostream& out)
{
    std::random_device dev;
    std::mt19937 gen(dev());
    std::uniform_int_distribution<int> dirDist(0, 3);
    size_t x = 0, y = 0;
    Direction dir = Right;
    char inst = '\0';
    std::stack<Int> stack;
    bool stringmode = false;
    while (inst != '@')
    {
        inst = get(source, x, y);
        if (stringmode)
        {
            if (inst == '"') stringmode = false;
            else stack.push(inst);
        }
        else
        {
            switch (inst)
            {
#define HANDLE_OP(op, c)\
            case c:\
            {\
                Int a = pop(stack);\
                Int b = pop(stack);\
                stack.push(b op a);\
                break;\
            }
            HANDLE_OP(+, '+');
            HANDLE_OP(-, '-');
            HANDLE_OP(*, '*');
            HANDLE_OP(/, '/');
            HANDLE_OP(%, '%');
#undef HANDLE_OP
            case '!': stack.push(!pop(stack)); break;
            case '`':
            {
                Int b = pop(stack);
                Int a = pop(stack);
                stack.push(a > b);
                break;
            }
            case '>': dir = Right; break;
            case '<': dir = Left; break;
            case '^': dir = Up; break;
            case 'v': dir = Down; break;
            case '?': dir = (Direction)dirDist(gen); break;
            case '_': dir = pop(stack) ? Left : Right; break;
            case '|': dir = pop(stack) ? Up : Down; break;
            case '"': stringmode = true; break;
            case ':': duplicate(stack); break;
            case '\\': swap(stack); break;
            case '$': pop(stack); break;
            case '.': out << (Int)pop(stack) << ' '; break;
            case ',': out << (char)pop(stack); break;
            case '#': move(source, x, y, dir); break;
            case 'g':
            {
                Int y_ = pop(stack);
                Int x_ = pop(stack);
                stack.push(get(source, x_, y_));
                break;
            }
            case 'p':
            {
                Int y_ = pop(stack);
                Int x_ = pop(stack);
                Int v = pop(stack);
                set(source, x_, y_, v);
                break;
            }
            case '&':
            {
                Int v = 0;
                in >> v;
                stack.push(v);
                break;
            }
            case '~':
            {
                char c = '\0';
                in >> c;
                stack.push(c);
                break;
            }
            case '0': case '1': case '2': case '3': case '4':
            case '5': case '6': case '7': case '8': case '9':
                stack.push(inst-'0');
                break;
            case '@': return;
            case ' ': break;
            default:
                std::cerr << "Unknown character '" << inst << "'.\n";
                std::exit(5);
            }
        }
        move(source, x, y, dir);
    }
}

Source extract(std::istream& in)
{
    std::string reader;
    Source src;
    size_t maxLen = 0;
    while (std::getline(in, reader))
    {
        src.push_back(reader);
        maxLen = std::max(maxLen, src.back().size());
    }
    while (!src.empty() && src.back().empty()) src.pop_back();
    return src;
}

int main(int argc, char* argv[])
{
    for (int i = 1; i < argc; ++i)
    {
        std::ifstream file(argv[i]);
        if (!file.is_open())
        {
            std::cerr << "Could not open \"" << argv[i] << "\" for reading.\n";
            return 2;
        }
        auto source = extract(file);
        interpret(source, std::cin, std::cout);
    }
    if (argc <= 1)
    {
        auto source = extract(std::cin);
        interpret(source, std::cin, std::cout);
    }
}

