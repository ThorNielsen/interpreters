#!/bin/bash

if [ ! -d bin ]; then
    mkdir bin
fi

# Befunge interpreter
g++ -std=c++11 -O3 -Wall -Wextra -Werror befunge.cpp -o bin/befunge

# Brainfuck interpreter
g++ -std=c++11 -O3 -Wall -Wextra -Werror brainfuck.cpp -o bin/brainfuck
